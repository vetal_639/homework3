<?php
class User
{
    public $name;
    public $age;
    private function setAge($age)
    {
        if ($this->isValidAge($age)) {
            $this->age = $age;
            return $age;
        }
    }

    public function addAge($plus)
    {
        $age=$this->age + $plus;
        return $age;
    }

    public function subAdge($minus)
    {
        $age=$this->age - $minus;
        return $age;
    }

    public function isValidAge($data):bool
    {
        return is_int($data);
    }
}
$user1=new User();
$user1->name="Витя";
$user1->age=34;
echo $user1->setAge(37);
echo '<br>';
echo $user1->addAge(3);
echo '<br>';
echo $user1->subAdge(2);
//выдает ошибку и ничего не считает....