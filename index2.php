<?php
//ЗАДАЧА 1
class Employee
{
    public $name;
    public $age;
    public $salary;

}
$employee1=new Employee();
$employee1->name='Иван';
$employee1->age=25;
$employee1->salary=1000;

$employee2=new Employee();
$employee2->name='Вася';
$employee2->age=26;
$employee2->salary=2000;
echo $employee1->salary+$employee2->salary;
echo '<br>';
echo $employee1->age + $employee2->age;

class Employee
{
    public $name;
    public $age;
    public $salary;
    public function getName()
    {
        return $this->name;
    }

    public function getAge()
    {
        return $this->age;
    }

    public function getSalary()
    {
        return $this->salary;
    }

    public function checkAge()
    {
        if ($this->age>18){
            return true;
        }
        else{
            return false;
        }
    }
}
$Employee1=new Employee();
$Employee1->name='Иван';
$Employee1->age=20;
$Employee1->salary=3700;

$Employee2=new Employee();
$Employee2->name='Игорь';
$Employee2->age=26;
$Employee2->salary=6500;
echo $Employee1->getSalary() + $Employee2->getSalary();
