<?php
class Student
{
    public $name;
    public $course;
    private $courseAdministrator;

    /*public function transferToNextCourse()
    {
        if($this->course<5){
            $course=$this->course+1;
                return $course;
        }
        else{
          echo "Стопе!Студент закончил обучение!";
        }
	}
    */
    private function isCourseCorrect()
    {
        if($this->course<5){
            $course=$this->course+1;
            return $course;
        }
    }

    public function setCourseAdministrator($name)
    {
        $this->courseAdministrator=$name;

    }

    public function getCourseAdministrator()
    {
        return $this->courseAdministrator;
    }

}
$student=new Student();
$student->name="Андрей";
$student->course=2;
//echo $student->transferToNextCourse();
//echo $student->isCourseCorrect();//выдает ошибку
//$student->courseAdministrator="Миша";//выдает ошибку так как метод является private -
// и phpstorm - даже не предлагает его вписать для вызова метода для объекта.
$student->setCourseAdministrator("Сергей");
$student->getCourseAdministrator();