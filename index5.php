<?php
class Rectangle
{
    public $width;
    public $height;
    public function getSquare()
    {
        $square=$this->width*$this->height;
        return $square;
    }

    public function getPerimeter()
    {
        $perimeter=$this->width*2+$this->height*2;
        return $perimeter;
    }

}

$rectangular= new Rectangle();
$rectangular->width=7;
$rectangular->height=3;

//echo $rectangular->getSquare();
//echo "<br>";
//echo $rectangular->getPerimeter();